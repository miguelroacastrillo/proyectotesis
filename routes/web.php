<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/tesistas', 'TesistaController@index');
//Route::resource('tesistas', TesistaController::class);
//Route::get('/tesistas','\App\Http\Controllers\TesistaController@index');

//Redireccion pagina principal
Route::redirect('/', 'tesistas'); //Tempralmente se muestran solo las tesis

Route::resource('tesis', 'App\Http\Controllers\TesisController');
Route::resource('tesistas','App\Http\Controllers\TesistaController');







