<?php

namespace App\Http\Controllers;

use App\Models\defensa;
use Illuminate\Http\Request;

class DefensaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\defensa  $defensa
     * @return \Illuminate\Http\Response
     */
    public function show(defensa $defensa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\defensa  $defensa
     * @return \Illuminate\Http\Response
     */
    public function edit(defensa $defensa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\defensa  $defensa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, defensa $defensa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\defensa  $defensa
     * @return \Illuminate\Http\Response
     */
    public function destroy(defensa $defensa)
    {
        //
    }
}
