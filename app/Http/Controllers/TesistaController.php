<?php

namespace App\Http\Controllers;

use App\Models\tesista;
use Illuminate\Http\Request;

class TesistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['tesistas']=Tesista::paginate(5);
        return view('tesistas.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tesistas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosTesistas=request()->except('_token');
        Tesista::insert($datosTesistas);
        return redirect('tesistas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tesista  $tesista
     * @return \Illuminate\Http\Response
     */
    public function show(tesista $tesista)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tesista  $tesista
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tesista=Tesista::findOrFail($id);
        return view('tesistas.edit', compact('tesista'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tesista  $tesista
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosTesista=request()->except(['_token', '_method']);
        Tesista::where('id','=', $id)->update($datosTesista);
        return redirect('tesistas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tesista  $tesista
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Tesista::destroy($id);
        return redirect('tesistas');
    }
}
