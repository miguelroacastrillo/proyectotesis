<?php

namespace App\Http\Controllers;

use App\Models\jurado_defensa;
use Illuminate\Http\Request;

class JuradoDefensaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jurado_defensa  $jurado_defensa
     * @return \Illuminate\Http\Response
     */
    public function show(jurado_defensa $jurado_defensa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jurado_defensa  $jurado_defensa
     * @return \Illuminate\Http\Response
     */
    public function edit(jurado_defensa $jurado_defensa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jurado_defensa  $jurado_defensa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jurado_defensa $jurado_defensa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jurado_defensa  $jurado_defensa
     * @return \Illuminate\Http\Response
     */
    public function destroy(jurado_defensa $jurado_defensa)
    {
        //
    }
}
