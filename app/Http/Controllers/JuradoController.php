<?php

namespace App\Http\Controllers;

use App\Models\jurado;
use Illuminate\Http\Request;

class JuradoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\jurado  $jurado
     * @return \Illuminate\Http\Response
     */
    public function show(jurado $jurado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\jurado  $jurado
     * @return \Illuminate\Http\Response
     */
    public function edit(jurado $jurado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\jurado  $jurado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jurado $jurado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\jurado  $jurado
     * @return \Illuminate\Http\Response
     */
    public function destroy(jurado $jurado)
    {
        //
    }
}
