<?php

namespace App\Http\Controllers;

use App\Models\tesis;
use App\Models\tesista;
use App\Models\tacademico;
use Illuminate\Http\Request;

class TesisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tesis.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        //para obtener todas los tesistas e imprimirlos en patnalla y elegir uno
        $tesistas = Tesista::all();
        $tacademicos = Tacademico::all();

        return view('tesis.create',compact('tesistas','tacademicos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //Para obtener todos los datos menos el token
        $datosTesis=request()->except('_token');

        Tesis::insert($datosTesis);

        return response()->json($datosTesis);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tesis  $tesis
     * @return \Illuminate\Http\Response
     */
    public function show(tesis $tesis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tesis  $tesis
     * @return \Illuminate\Http\Response
     */
    public function edit(tesis $tesis)
    {
        //
        return view('tesis.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tesis  $tesis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tesis $tesis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tesis  $tesis
     * @return \Illuminate\Http\Response
     */
    public function destroy(tesis $tesis)
    {
        //
    }
}
