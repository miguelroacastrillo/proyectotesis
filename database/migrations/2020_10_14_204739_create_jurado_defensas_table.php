<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJuradoDefensasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurado_defensas', function (Blueprint $table) {
            $table->increments('id');
            //fk
            $table->integer('Jurado_id')->unsigned();
            $table->foreign('Jurado_id')->references('id')->on('jurados')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            //fk
            $table->integer('Defensa_id')->unsigned();
            $table->foreign('Defensa_id')->references('id')->on('defensas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurado_defensas');
    }
}
