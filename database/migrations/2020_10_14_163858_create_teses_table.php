<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teses', function (Blueprint $table) {

            $table->increments('id');
            //fk
            $table->integer('tesista_id')->default(0);            
            $table->foreign('tesista_id')->references('id')->on('tesistas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            //----
            $table->string('titulo');
            $table->date('periodoInicio');
            $table->date('periodoFinal');
            $table->string('status');
            $table->string('empresa');
            //fk
            $table->integer('tacademico_id')->default(0);
            $table->foreign('tacademico_id')->references('id')->on('tacademicos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            //----
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teses');
    }
}
