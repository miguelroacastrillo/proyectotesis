<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefensasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defensas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            
            //fk
            $table->integer('Tesis_id')->unsigned();
            $table->foreign('Tesis_id')->references('id')->on('teses')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('numero_de_aula');
            $table->string('periodo_universitario');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defensas');
    }
}
