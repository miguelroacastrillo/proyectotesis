<table class="table table-light">
<thead class="thead-light">
    <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Cedula</th>
        <th>Carrera</th>
        <th>Telefono</th>
        <th>Acciones</th>
    </tr>
</thead>

<tbody>
@foreach($tesistas as $tesista)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$tesista->nombre}}</td>
        <td>{{$tesista->apellido}}</td>
        <td>{{$tesista->cedula}}</td>
        <td>{{$tesista->carrera}}</td>
        <td>{{$tesista->telefono}}</td>
        <td>
        <a href="{{ url('/tesistas/'.$tesista->id.'/edit')}}">Editar</a> |
        
        <form method="post" action="{{ url('/tesistas/'.$tesista->id)}}">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <button type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
        </form>
        </td>
    </tr>
@endforeach        
</tbody>