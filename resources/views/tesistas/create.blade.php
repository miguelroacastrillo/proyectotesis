<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Estilos para las web-->    
    <link rel="stylesheet" href="..\css\estilos.css">
    <link rel="stylesheet" href="..\css\normalize.css">
    <title>UA - Departamente de tesis</title>
</head>
<body>
<header>
        <div class="containerLogo">
            <a href="http://localhost/proyectotesis/public">
                <img src="../img/logo-blanco.png" alt="" class="imagenLogo">
            </a>        
        </div>
        <div class="menu">
            <ol>
                <li class="opcion">
                    <a href="#">
                        Home
                    </a>
                </li>
                <li class="opcion">
                    <a href="#">
                        Quienes Somos
                    </a>
                </li>
                <li class="opcion">
                    <a href="#">
                        Tesis
                    </a>
                </li>
            </ol>
        </div>
    </header>
    <section class="slider">
        <!--SliderOpcional para la pagina web-->
    </section>
    <section class="servicioTesis">
        <div class="mensaje">
            <h1>¡Bienvenido al sistema de tesis!</h1>
            <h4>Ingresa los datos para subirlos al sistema</h4>            
            <hr class="divisor">
        </div>        
        <div class="containerForm">            
            <h1 class="titulo">
                Crea a un tesista
            </h1>

            <form action="{{ url('/tesistas')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="entradasDatos">
                <div class="datosBox">
                    <label form="nombre">{{'Nombre'}}</label>
                    <input type="text" name="nombre" id="nombre" value="">
                </div>
                <div class="datosBox">
                    <label form="">{{'Apellido'}}</label>
                    <input type="text" name="apellido" id="apellido" value="">
                </div>
                <div class="datosBox">
                    <label form="nombre">{{'Cedula'}}</label>
                    <input type="text" name="cedula" id="cedula" value="">            
                </div>
                <div class="datosBox">
                    <label form="nombre">{{'Carrera'}}</label>
                    <input type="text" name="carrera" id="carrera" value="">
                </div>
                <div class="datosBox">
                    <label form="nombre">{{'Telefono'}}</label>
                    <input type="text" name="telefono" id="telefono" value="">
                </div>  
            </div>      
            <input type="submit" value="Agregar">
            </form>
        </div>
    </section>

</body>
</html>

