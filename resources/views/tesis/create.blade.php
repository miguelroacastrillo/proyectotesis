<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Estilos para las web-->    
    <link rel="stylesheet" href="..\css\estilos.css">
    <link rel="stylesheet" href="..\css\normalize.css">
    <title>UA - Departamente de tesis</title>
</head>
<body>
<header>
        <div class="containerLogo">
            <a href="http://localhost/proyectotesis/public">
                <img src="../img/logo-blanco.png" alt="" class="imagenLogo">
            </a>        
        </div>
        <div class="menu">
            <ol>
                <li class="opcion">
                    <a href="#">
                        Home
                    </a>
                </li>
                <li class="opcion">
                    <a href="#">
                        Quienes Somos
                    </a>
                </li>
                <li class="opcion">
                    <a href="#">
                        Tesis
                    </a>
                </li>
            </ol>
        </div>
    </header>
    <section class="slider">
        <!--SliderOpcional para la pagina web-->
    </section>
    <section class="servicioTesis">
        <div class="mensaje">
            <h1>¡Bienvenido al sistema de tesis!</h1>
            <h4>Ingresa los datos para subirlos al sistema</h4>            
            <hr class="divisor">
        </div>        
        <div class="containerForm">            
            <h1 class="titulo">
                Crea a una tesis
            </h1>

            <form action="{{ url('/tesis')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}} <!--Para el token de envio-->
            <div class="entradasDatos">
                <div class="selectBox">
                    <label form="nombreTesistas">{{'Nombre del tesista'}}</label>
                    <select name="nombreTesistas" id="nombreTesistas">
                        @foreach ($tesistas as $tesista)
                            <option>{{$tesista->nombre}}</option>
                        @endforeach
                    </select>                    
                </div>
                <div class="datosBox">
                    <label form="titulo">{{'Titulo'}}</label>
                    <input type="text" name="titulo" id="titulo" value="">
                </div>
                <div class="datosBox">
                    <label form="periodoInicio">{{'Inicio del periodo'}}</label>
                    <input type="date" name="periodoInicio" id="periodoInicio" value="">
                </div>
                <div class="datosBox">
                    <label form="periodoFinal">{{'Final del periodo'}}</label>
                    <input type="date" name="periodoFinal" id="periodoFinal" value="">            
                </div>
                <div class="datosBox">
                    <label form="status">{{'Status'}}</label>
                    <input type="text" name="status" id="status" value="">
                </div>
                <div class="datosBox">
                    <label form="empresa">{{'Empresa'}}</label>
                    <input type="text" name="empresa" id="empresa" value="">
                </div>  
                <div class="selectBox">
                    <label form="tacademicos">{{'Tutor académico'}}</label>
                    <select name="tacademicos" id="tacademicos">
                        @foreach ($tacademicos as $tacademico)
                            <option>{{$tacademico->nombre}}</option>
                        @endforeach
                    </select>                    
                </div>
            </div>      
            <input type="submit" value="Agregar">
            </form>
        </div>
    </section>

</body>
</html>

